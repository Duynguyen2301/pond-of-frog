import { combineReducers } from 'redux';
import frog from 'reducers/frog';

const rootReducer = combineReducers({
  frog
});

export default rootReducer;
