import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import serviceFrog from '../middleware/frogs';

const Listing = React.createClass({

  getInitialState: function(){
    return {
      frogs: [],
      isFrogsLoading: true
    };
  },

  componentDidMount() {
    //Call API to get frog listing
    serviceFrog.list(function(res){
      this.setState({
        frogs: res.body.result,
        isFrogsLoading: false,
        isFrogsLoaded: true
      });
    }.bind(this), function(err){
      console.log('err', err);
    }.bind(this));
  },
  
  handleDelete(item, index, e) {
    e.preventDefault();
    if(confirm("Are you sure you want to delete?")){
      //Call API to delete frog listing
      serviceFrog.del(item._id, function(res){
        var frogs = this.state.frogs;
        frogs.splice(index, 1);
        this.setState({frogs: frogs});
      }.bind(this), function(err){
        console.log('err', err);
      }.bind(this));
    }
  },  
  
  render: function() {
    return (
      <div className="listing">
        <h3>Frogs listing</h3>
        <hr/>
        {this.state.isFrogsLoading &&
          <i className="glyphicon glyphicon-refresh spinning"></i>
        }
        {this.state.isFrogsLoaded &&
          <table className="table table-bordered">
            <thead>
              <tr>
                <th>Name</th>
                <th>Gender</th>
                <th>Birth</th>
                <th>Pond Environment</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {!this.state.frogs &&
                <tr>
                  <td colSpan>Not found.</td>
                </tr>
              }
              {this.state.frogs && this.state.frogs.map(function(item, i){
                return (
                  <tr key={i}>
                    <td>{item.name}</td>
                    <td>{item.gender}</td>
                    <td>{item.birth}</td>
                    <td>{item.pond_environment}</td>
                    <td>
                      <a href={"/frogs/" + item._id}> <i className="glyphicon glyphicon-eye-open"></i> </a>
                      <a href={"/frogs/edit/" + item._id}> <i className="glyphicon glyphicon-pencil"></i> </a>
                      <a href="#" onClick={this.handleDelete.bind(this, item, i)}> <i className="glyphicon glyphicon-remove"></i> </a>
                    </td>
                  </tr>
                )
              }, this)}
            </tbody>
          </table>
        }
      </div>
    );
  }
});

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(Listing);