import React from 'react';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute } from 'react-router';
import configureStore from 'store/configureStore';

import App from 'containers/App';
import Listing from 'containers/Listing';
import Form from 'containers/Form';
import Detail from 'containers/Detail';

export default function(history) {
  return (
    <Router history={history}>
      <Route path="/" component={App}>
        <Route path="frogs/add" component={Form} />
        <Route path="frogs/edit/:id" component={Form} />
        <Route path="frogs/:id" component={Detail} />
        <IndexRoute component={Listing} />
      </Route>
    </Router>
  );
};
